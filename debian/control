Source: tse3
Section: sound
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 12),
               dpkg-dev (>= 1.22.5),
               libasound2-dev [linux-any],
Standards-Version: 4.7.0
Homepage: http://tse3.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/debian/tse3
Vcs-Git: https://salsa.debian.org/debian/tse3.git

Package: libtse3-0.3.1t64
Provides: ${t64:Provides},
X-Time64-Compat: libtse3-0.3.1c2a
Breaks: libtse3-0.3.1c2a (<< ${source:Version}),
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Conflicts: libtse3-0.3.1,
Replaces: libtse3-0.3.1,
          libtse3-0.3.1c2a,
Description: portable MIDI sequencer engine in C++
 TSE3 is a portable open source MIDI sequencer engine written in C++.
 It provides programmers with rich and powerful sequencing
 capabilities.  Although it does not provide a user interface, it
 provides a lot of assistance to a UI, attempting to provide as much
 functionality as possible in as generic a manner as possible.
 .
 This package provides the shared library libtse3, which is required
 in order to run programs linked against tse3.

Package: libtse3-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libtse3-0.3.1t64 (= ${binary:Version}),
         ${misc:Depends},
Description: portable MIDI sequencer engine in C++ - development files
 TSE3 is a portable open source MIDI sequencer engine written in C++.
 It provides programmers with rich and powerful sequencing
 capabilities.  Although it does not provide a user interface, it
 provides a lot of assistance to a UI, attempting to provide as much
 functionality as possible in as generic a manner as possible.
 .
 This package provides the headers and static libraries that are
 needed to compile something that uses tse3. It is not required to run
 programs that are linked against tse3.

Package: tse3play
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: MIDI/TSE3MDL player/converter
 tse3play plays TSE3MDL files and MIDI files using the TSE3 sequencer
 engine.
 .
 It can convert files between the two supported formats. While playing
 it provides text-based visual feedback and can stream an English
 representation of the contents of the file to standard output.
